---
title: "Cashless Consumer Technology"
description: "All technology initiatives towards Cashless Consumer"
date: 2018-03-07T02:16:58-08:00
draft: false
categories: ['tech',]
tags: ['tech',]
---

---
Technology is a key part of Digital payments or broadly FinTech and increasingly indispensable part of every digital payments system. To  protect consumer interests, one needs deep knowledge about existing systems, see the policy / regaultion through the prism of technology, understand, evaluate technology cost, benefits, risks from consumer perspective.

# Interests

* Understanding technology behind payment standards, protocols, systems, networks, services with a view to detect anti consumer practices.
* Push towards better and open standards, participate in development of the same.
* Review payment technologies, applications to check if they are compliant with regulations.
* Look out for bugs, security holes in payment technologies, report them responsibly.

# Work

* [SDK Universe](https://gitlab.com/CashlessConsumer/sdkuniverse) - Mirror of all SDKs / APIs by Fintechs in India from their official repositories
* [Koodous](https://koodous.com/analysts/cashlessconsumer) - [OSINT](https://en.wikipedia.org/wiki/Open-source_intelligence) Detection and Security analysis of payment apps.
* [Incident Reports](post/incidentreports) - Reporing Security / Privacy issues in consumer payment systems.
* [Understanding UPI](https://cashlessconsumer.github.io/CashlessConsumer/), continous feedback on UPI.
* [AppStoreMonitor](https://gitlab.com/CashlessConsumer/AppStoreMonitor/) - App Store data collector.
* Technical support for all Cashless Consumer initiatives.
