---
title: "Cashless Consumer Awareness"
description: "All awareness initiatives towards Cashless Consumer"
date: 2018-08-22T14:36:58+05:30
draft: false
categories: ['awareness']
tags: ['awareness']
---

---

# Interests

* Holistic understanding of payments, going beyond digital literacy.
* Inform consumer risks swiftly, widely.
* Be informed consumer to make informed choices.
* Voice perspectives and make other stakeholders aware of consumer concerns.
* Awareness to new development in the payments space.

# Work

* [River of Feeds](https://cashlessconsumer-river5.glitch.me/) tracking Indian PayTech containing [Newsfeeds](http://newsrack.in/topics/srikanthlogic/Digital+Payments),Industry Blogs and Other Sources - [OPML File](uploads/subscriptions.xml) powered by [Glitch](http://www.glitch.com), [Newsrack](http://newsrack.in) and [FeedReader](https://feedreader.com/)
* [Articles in Media](post/opinions) - Opinion pieces, Explainer articles on contemporary events in digital payments landscape
* [Media quotes](post/mediaquotes) - Quotes on media articles covering digital payments giving in a consumer perspective.
* [Ethical Framework for evangelism of Cashless Consumer apps](https://commons.wikimedia.org/wiki/File:Ethicalframeworkforevangelisationofcashlessconsumerapps.pdf) - Chinmayi SK - Dec 24, 2016 - Wikimedia Commons