---
title: "Incident Reports"
description: ""
date: 2019-03-31T09:04:20+05:30
categories: []
tags: ["Tech","Privacy","Security"]
draft: false
---

# Incident Reports

As part of monitoring the payments ecosystem, we help in reporting privacy, security issues and help entities fix them. While we prefer responsible disclosure, we might disclose critical issues publicly as well. If you find something wrong / concerning, please let us know through [contact](about/contact) and we will assist you in reporting.

| S.No | Entity | Type of Report | Outcome | Comments |
|---|---|---|---|---|
|1| Aditya Birla Payments Bank | Data Leak - [Report](https://twitter.com/logic/status/1024641546799464451) | Fixed | Directory browsing enabled on webserver leaking Aadhaar / transaction data |
|2|  UltraCash | Malware Detection - [Report](https://twitter.com/logic/status/1100305395069972480) | Fixed | Found SMS Malware in multiple versions of app |
|3| PayUMoney | Data Leak - [Report](https://twitter.com/logic/status/1106413443530129409) | Fixed | Unauthenticated users can access partial de-tokenized card information of users |
<!--more-->
