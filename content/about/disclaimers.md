---
title: Disclaimers
description: Short set of policies, guidelines, disclaimers
date: 2018-06-13
updated: 2018-06-13
layout: subsection
slug: disclaimers
weight: 103
---

## Policies, Guidelines, Disclaimers

* We believe in opendata, free licensing. Unless specified, all works of CashlessConsumer are freely licensed.
* Unless specified, all content are licensed under [CC-BY](https://creativecommons.org/licenses/by/2.0/), data licensed under [CC0](https://wiki.creativecommons.org/wiki/CC0) and code licensed under [MIT](https://opensource.org/licenses/MIT).
* We are a collective, NOT an organization, NOT a legal entity, by extension have no financial interests. Individuals contribute (mostly time, efforts) to collective under their personal capacity and views, opinions are personal. While providing feedback as a collective, we would be inclusive in collecting / gathering all feedback and seek consensus. Members of collective can disagree on matters.
* While the information we provide is verified to best of our abilities, we do not offer any warrenty on the same, please verify yourself.
* We do not collect any personal data, do not collect analytics / web statistics, hence do not have a privacy policy.
