---
title: "Cashless Consumer Data"
description: "All data initiatives towards Cashless Consumer"
date: 2018-03-07T02:16:58-08:00
draft: false
categories: ['data',]
tags: ['data',]
---

---

Data is a new oil. We need clear understanding of data economy and role of payments in data economy, data practices, rights, benefits and risks of data first system, use of data to assist governance, regulation, improve competition. We believe in opendata for good and try our bit in making data available for understanding the ecosystem.

# Interests

* Role of consumer data used in payment technologies.
* Seek more opendata in payment technologies, to better serve policy.
* Collect, organize data, in machine friendlier formats where available.
* Generate opendata where possible to further data driven discourse.
* Analyse, visualize data to gather insights to increase understanding.

# Work

* [RTI Desk](post/rti) - Using RTI to drive transparency in regulation of digital payments, promotion of digital payments.
* [Generating Opendata](post/opendatabycc) - Generating / Collecting data related to digital payments.