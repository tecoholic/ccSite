---
date: 2018-03-01
draft: false
title: cashlessconsumer-index
---

CashlessConsumer is a consumer collective working on digital payments to increase 
awareness, understand technology, produce / consume data, represent consumers in policy of digital payments ecosystem to voice consumer perspectives, concerns with a goal of moving towards a fair cashless society

<p style="display: none;"><a href="https://freeradical.zone/@cashlessconsumer" rel="me">Mastodon</a></p>
