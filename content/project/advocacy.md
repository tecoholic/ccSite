---
title: "Cashless Consumer Advocacy"
description: "All advocacy initiatives towards Cashless Consumer"
date: 2018-09-24T02:16:58-08:00
draft: false
categories: ['advocacy']
tags: ['advocacy']
---

---

Advocacy involves reaching out to more people through participation in community meetups, talks at seminars, conferences, workshops. Please [Contact](http://cashlessconsumer.in/about/contact) for speaking requests in your events.

# Talks

|Type|Topic|Event|Date|Video / Slides|
|---|---|---|---|---|
| Community Meetup | Cashless Consumer - Why?  | [Lamakaan](http://web.archive.org/web/20180924115451/http://admin.lamakaan.com/events/3680) | 24 Dec 2016 |  [Slides](https://drive.google.com/open?id=1VWtXcDgyLOgDR-f2b32zhJGuMyG6RRgzoRtQk8mlRsI)|
| Talk | UPI & Beyond - How communities can help consumers in cashless world | [50p 2017](https://50p.in/2017/) | 24 Jan 2017 | [Video](https://www.youtube.com/watch?v=ZeolwOp9sk8) / [Slides](https://docs.google.com/presentation/d/12mQz9OlP5WrGdv4fTxQkY1ikXUgClsrh60ulSRHwy6g/edit?usp=sharing)|
| Workshop | CIS Digital Payments Master class 2017 || 26 Jan 2017 | |
| Panel | Demonetisation and Digital Infrastructure | [4ccon](https://fsftn.gitlab.io/4ccon/) | 27 Jan 2017 | [Video](https://www.youtube.com/watch?v=U60Pj0kyiHQ)|
| Talk | Know your App - App Economy Accountability | [Devcon Hyderabad](http://devconhyd.com/) | 14 Oct 2017 | [Slides](https://docs.google.com/presentation/d/1LdnzSzPsKQFJqubdpvS1v5mCNTZjh3TSQLQZjpMptuo)|
| Talk | State of UPI - Observations from reluctant user | [50p 2018](https://50p.in/2018/) | 8 Feb 2018 | [Video](https://www.youtube.com/watch?v=Z7ydlOyjIWY) / [Slides](https://docs.google.com/presentation/d/1EfcsouyCFiM7gugesTmTcjAyBjLiceG2nnng1gUW0j4/edit?usp=sharing)|
| Workshop | CIS Digital Payments Master class 2018 || 11 Feb 2018 | |
| Talk | State of UPI | [NIPFP](https://nipfp.org.in/home-page/) Outreach | 15 Jun 2018 | [Slides](https://drive.google.com/open?id=1wBRX-LKxAsJC0ibhxh1xwN132yX4WpE7zlVPLSbrNDs)|
| Panel | Data protection and surveillance | [Nalsar Tech Law Forum](https://www.facebook.com/events/731709047163280/) | 09 Sep 2018 | [Video](https://www.youtube.com/watch?v=QraT867Lg6U)|
| Panel | Privacy, Data protection, Data localisation & Aadhaar | [Razorpay FTX](https://razorpay.com/ftx/) | 07 Dec 2018 | |
| Community Meetup | Discussion on RBIs Policy Paper on Authorisation of New Retail Payment Systems |[Hasgeek](https://hasgeek.com/50p/discussion-rbi-policy-paper-retail-payments-2019/) | 15 Feb 2019 | [Slides](https://docs.google.com/presentation/d/1Z0UUo3FEmzTWojogGXRW7SUUE_f1VXACTc0DWu6fvU4/edit?usp=sharing) |
| Workshop | Data and Consent in Financial Sector | [CUTS](https://www.cuts-ccier.org) Workshop | 19 Jul 2019 | [Slides](https://docs.google.com/presentation/d/1IAwq_6_RR94rY01CHmn5tU5_Vtz6Mrf0JCTbnPSdL10/edit)|
| Round Table | Privacy protection policy of India: Will the consumers feel empowered or enraged? | [CUTS](https://www.cuts-ccier.org) Workshop | 19 Jul 2019 | |