---
title: "RTI Desk - List of RTI"
description: ""
date: 2018-10-23T09:04:20+05:30
categories: ["rti","rtireply"]
tags: ["rti"]
draft: false
---

# List of RTIs

------------------------
| S.No | RTI ID | Date Filed | PIO | Content | Response Status | Summary |
|---|---|---|---|---|---|---|
|[RTI-001](post/rti-001)| RBIND/R/2018/54595 | 14/09/2018    | Reserve Bank of India ([RBI](https://www.rbi.org.in))| Cosmos Hack, Card network penalties | REQUEST DISPOSED OF (12/10/2018)| 8(1)(a) - No further details, No penalty |
|[RTI-002](post/rti-002)| FINIU/R/2018/50029 | 23/10/2018    | Financial Intelligence Unit | Data localisation, Aadhaar and Penalties on PPI | REQUEST DISPOSED OF as on 24/10/2018| 24(1) National security exemption of agency |
|[RTI-003](post/rti-003)| RBIND/R/2018/54884 | 04/10/2018    | RBI | Banking Fraud Awareness campaign for consumers  | REQUEST DISPOSED OF (01/11/2018)| Information Provided |
|[RTI-004](post/rti-004)| RBIND/R/2018/55014 | 12/10/2018    | RBI | NBFC Account Aggregator licenses  | REQUEST DISPOSED OF   (06/11/2018)| Partial Information Provided, 1st Appeal filed |
|[RTI-005](post/rti-005)| NABRD/R/2018/51006 | 12/10/2018    | National Bank for Agriculture and Rural Development | Usage of Financial Inclusion Fund by NABARD for Improving infrastructure  | REQUEST DISPOSED OF (09/11/2018)| Information Provided |
|[RTI-006](post/rti-006)| CICOM/R/2018/50827 | 25/10/2018    | Central Information Commission | Guidelines to CIC judgement structure, List of entities excluded from RTI by CIC  | REQUEST DISPOSED OF (05/11/2018)| Information not available |
|[RTI-007](post/rti-007)| MOCAF/R/2018/50794 | 23/10/2018    | Ministry of Corporate Affairs | Use of National in naming entities and special permission  | REQUEST DISPOSED OF (01/11/2018)| Information Provided |
|[RTI-008](post/rti-008)| RBIND/R/2018/55078 | 16/10/2018    | RBI |  Directions on KYC, Payments Systems after Aadhaar Verdict | REQUEST DISPOSED OF   (16/11/2018)| Partial Information Provided |
|[RTI-009](post/rti-009)| CFRIS/R/2018/50341 | 25/10/2018    | Centre for Railway Information System (CRIS) |  Discounts, Usage of BHIM UPI in rail ticket booking | REQUEST DISPOSED OF (20/11/2018)| Information Provided |
|[RTI-010](post/rti-010)| RBIND/R/2018/55230 | 24/10/2018    | RBI |  Clarification on AePS pricing related regulation | REQUEST DISPOSED OF   (22/11/2018)| Information Provided |
|[RTI-011](post/rti-011)| SEBIH/R/2018/50707 | 22/10/2018    | Securities and Exchange Board of India |  Public Consultation on IPO Listing Process UPI | REQUEST DISPOSED OF (20/11/2018)| Information Not Provided, Appeal Filed |
|[RTI-012](post/rti-012)| CONCA/R/2018/50020 | 16/10/2018    | Controller of certifying Authorities (CCA) |  Regulatory clarity on use of eSign after Aadhaar Verdict | REQUEST DISPOSED OF (19/11/2018)| Information Provided |
|[RTI-013](post/rti-013)| ISBBI/R/2018/50110 | 23/10/2018    | Insolvency and Bankruptcy Board of India (IBBI) | Insolvency and Bankruptcy Board of India - Regulatory Governance | REQUEST DISPOSED OF (20/11/2018)| Information Provided |
|[RTI-014](post/rti-014)| RBIND/R/2018/55380 | 05/11/2018    | RBI | Public Credit Registry - Regulatory Governance | REQUEST DISPOSED OF (26/11/2018)| No Information Available |

* [RTI Desk](post/rti)
* [File RTI using RTI Desk](post/filerti)
* [All RTI Replies](categories/rtireply)
