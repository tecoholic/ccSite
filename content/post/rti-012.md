---
title: "RTI Desk - RTI-012"
description: "RTI 012 - Regulatory clarity on use of eSign after Aadhaar Verdict"
date: 2018-11-28T17:04:20+05:30
categories: ["rtireply"]
tags: ["RTI", "CCA", "Aadhaar", "eSign", "eMandate", "Regulatory Governance"]
draft: false
---

# RTI 012 - Regulatory clarity on eSign after Aadhaar Verdict

------------------------
| Field | Value |
|---|---|
| RTI ID | CONCA/R/2018/50020 |
| Date of Filing | 16/10/2018 |
| PIO | Controller of certifying Authorities (CCA) |
| Subject | Regulatory clarity on use of eSign after Aadhaar Verdict |
| Query | Further to the Aadhaar judgement by the Supreme Court constitutional bench Please provide information on updates to rules, regulations made by CCA related to eSign Service Providers.<br><br>1. Whether CCA approves use of Aadhaar authentication / eKYC by Private eSign Service Providers (ESP)<br><br>2. if so under what law or regulation<br><br>3. if not, please provide details of steps taken by CCA in cancelling the licenses of private eSign providers. |
| Response | 1. As a requirement for e-authentication using e-KYC services, ESP should adhere to e-KYC compliance requirements independently. The applicable e-KYC service provider for eSign is UIDAI (Aadhaar e-KYC services). <br><br> 2. -do- <br><br> 3. Other than CA license, no seperate license has been given to eSign service providers (ESP) |
| Date of Response | 13/11/2018 |
| Twitter | [Tweet](https://twitter.com/logic/status/1066911815773302784) |
| Notes | RTI, CCA, Aadhaar, eSign, eMandate  |

* [RTI Desk](post/rti) 
* [File RTI using RTI Desk](post/filerti) 
* [List of RTI](post/rtilist) 
* [All RTI Replies](tags/rtireply)