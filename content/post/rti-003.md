---
title: "RTI Desk - RTI-003"
description: "RTI 003 - Banking Fraud Awareness campaign for consumers"
date: 2018-11-01T17:04:20+05:30
categories: ["rtireply"]
tags: ["RBI", "Awareness", "Campaign"]
draft: false
---

# RTI 003 - Banking Fraud Awareness campaign for consumers

------------------------
| Field | Value |
|---|---|
| RTI ID | RBIND/R/2018/54884|
| Date of Filing | 04/10/2018 |
| PIO | Reserve Bank of India |
| Subject | Banking Fraud Awareness campaign for consumers |
| Query | 1. Please provide monthly breakup and total number of SMS sent,along with distinct cautionary message templates sent from RBISAY sender id alerting members of public.<br><br> 2. Please provide monthly breakup and total number of missed calls from members of public to 8691960000 seeking information through RBISAY <br><br>3. Please provide monthly breakup and total number of emails received from members of public to email ID - rbikehtahai@rbi.org.in |
| Response | 1. The department does not maintain a month-wise record of the SMS campaign. The message wise campaign details are enclosed in Annexure 'A' <br><br> 2. Same as above. It is also informed that 8691960000 was the missed call number for the first two messages. The number was subsequently replaced with short code 14400. Rest of the seven SMSes had '14400' as the 'call to action number'. The details of missed calls received is attached in Annexure 'A' <br><br> 3. The department does not maintain a monthly record of emails received on rbikehtahai. 655 emails have been received on the mail id till now starting November 2017. <br><br>[Reply](https://drive.google.com/open?id=1yVEZddT9NrAtru9fgSgCf0pB64eUKb9e) -- [Annexure 'A'](https://drive.google.com/open?id=1QUz61bRzsPweosaOmmqYeCTf34UL6pgG)|
| Date of Response | 01/11/2018 |
| Twitter | [Tweet](https://twitter.com/logic/status/1057957809806102528) |
| Notes | #RBI, #Awareness, #Fraud, #Campaign |

* [RTI Desk](post/rti)
* [File RTI using RTI Desk](post/filerti)
* [List of RTI](post/rtilist)
* [All RTI Replies](tags/rtireply)