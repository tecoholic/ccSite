---
title: "Policy Paper on Retail Payments Authorisation"
description: "RBI's Policy Paper on Retail Payments Authorisation"
date: 2019-04-26T09:04:20+05:30
categories: ["awareness", "policy"]
tags: ["policy","consultation","policytracker"]
draft: false
---

# Introduction

On Jan 21,2019 Reserve Bank of India put out a policy paper on authorisation of retail payment systems, with a view to minimize the concentration risk in retail payment systems, from a financial stability perspective and to foster innovation and competition.

* [Announcement](https://rbi.org.in/scripts/PublicationReportDetails.aspx?UrlPage=&ID=918)
* Twitter Hashtag - [#RetailPaymentsAuthorization](https://twitter.com/hashtag/RetailPaymentsAuthorization)

# Media Articles
* [RBI Encourages More Payments Companies To Enter Country’s Fintech Market](https://inc42.com/buzz/rbi-invites-comments-on-draft-authorisation-of-new-retail-payment-systems/), 22 January 2019, Inc42
* [RBI should drive the initiative to set up new NPCI: former MD Hota](https://www.livemint.com/politics/policy/rbi-should-drive-the-initiative-to-set-up-new-npci-former-md-hota-1549222103153.html), 04 February 2019, Livemint
* [Global ePayment firms make a wishlist for RBI](https://tech.economictimes.indiatimes.com/news/startups/global-epayment-firms-make-a-wishlist-for-rbi/68712071), 04 April, 2019, Economic Times
* [Independence in payments regulation – Is this the need of the hour?](https://www.fortuneindia.com/opinion/independence-in-payments-regulation-is-this-the-need-of-the-hour/103122), 11 April, 2019, Fortune India

# Policy Perspetives / Submissions

* [Business Correspondants Federation of India Submission](http://bcfi.org.in/wp-content/uploads/2019/02/RBI-Retail-Payment-Policy-Systems-Feedback-040219.pdf), 04 February 2019, BCFI

* [CashlessConsumer Submission]
(https://docs.google.com/document/d/e/2PACX-1vQ890MrmxeXC9UCfJgVndSEXYdeBB4iCnM7rGLxExMUhXOH6dc5EP3nUvoApqleoJyFFARTbEUpXv9J/pub), 20 February 2019, CashlessConsumer
