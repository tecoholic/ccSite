---
title: "Opinions"
description: "List of media articles, opinion pieces"
date: 2018-10-28T09:04:20+05:30
categories: ["awareness", "policy"]
tags: ["media","opinon"]
draft: false
---

We give consumer perspectives to media stories writing about digital payments. If you are journolist and want quotes, please [contact](about/contact) us for quotes for your stories.

# Media

* [Bhim app may have topped download charts but it's not replacing cash (or even Paytm) just yet](https://scroll.in/article/825809/bhim-app-may-have-topped-download-charts-but-its-not-replacing-cash-or-even-paytm-just-yet) - Jan 5, 2017 - Scroll
* [No updates for 50% UPI apps in 50 days](https://timesofindia.indiatimes.com/trend-tracking/no-updates-for-50-upi-apps-in-50-days/articleshow/56827144.cms) - Jan 28, 2017 - Times of India
* [When it comes to UPI apps, it’s PhonePe vs. BHIM](https://factordaily.com/upi-app-phonepe-bhim/) - Feb 10, 2017 - FactorDaily
* [Are the terms and conditions of BHIM-Aadhaar anti-consumer or simply anti-interpretation?](https://www.newslaundry.com/2017/04/20/are-the-terms-and-conditions-of-bhim-aadhaar-anti-consumer-or-simply-anti-interpretation) - Apr 20, 2017 - Newslaundry
* [NPCI asks banks to reject UPI transactions from non-compliant apps](https://www.business-standard.com/article/finance/npci-asks-banks-to-reject-upi-transactions-from-non-compliant-apps-118031900297_1.html) - Mar 19, 2018 - Business Standard
* [Tech Fraud: Conmen steal goods with fake payment msgs](https://www.deccanchronicle.com/nation/current-affairs/290418/tech-fraud-conmen-steal-goods-with-fake-payment-msgs.html) - Apr 29, 2018 - Deccan Chronicle
* [Why NPCI and Facebook need urgent regulatory attention](https://economictimes.indiatimes.com/industry/banking/finance/banking/why-npci-and-facebook-need-urgent-regulatory-attention/articleshow/64522587.cms) - Sunil Abraham, Jun 10, 2018 - Economic Times
* [Transaction declined: The tussle between the RBI and payments companies goes beyond data localisation](https://prime.economictimes.indiatimes.com/news/66438604) - Oct 31, 2018, ETPrime
* [Should NPCI be Under RTI? Central Information Commission to Decide](https://www.thequint.com/news/india/npci-to-be-under-rti-cic-to-decide) - Nov 2, 2018, The Quint
* [Public Comments on Listing Process Using UPI Affects 'Economic Interest' of India: SEBI](https://www.moneylife.in//article/public-comments-on-listing-process-using-upi-affects-economic-interest-of-india-sebi/55814.html) - Nov 27, 2018, Moneylife
* [Is the Indian central bank falling for lobbyists in seeking to widen digital transactions?](https://qz.com/india/1517823/nilekanis-ispirt-shadow-on-indias-rbi-digital-payments-panel/) - Jan 9, 2019, Quartz
* [These five companies quietly got the RBI's new "account aggregator" licence. But pulling financial data alone doesn't make a good business.](https://prime.economictimes.indiatimes.com/news/68295746) - Mar 7, 2019, ETPrime
* [Committee StoryThe government has set up committees galore to shape the fintech space. But their recommendations remain mostly on paper.](https://prime.economictimes.indiatimes.com/news/68398683) - Mar 14, 2019, ETPrime
* [Glitch in PayUMoney Gateway Exposes Users’ Credit Card Details](https://www.thequint.com/tech-and-auto/payumoney-gateway-exposes-users-credit-card-details) - Mar 15, 2019, The Quint
* [Consent brokers: India’s new data-sharing model can be a game-changer but has several loose ends](https://factordaily.com/consent-brokers-indias-new-data-sharing-model-can-be-a-game-changer-but-has-several-loose-ends/) - Mar 25, 2019, FactorDaily
* [How Sai Baba Was Made To Spy On Your Phone For Credit Ratings](https://www.huffingtonpost.in/entry/fintech-apps-privacy-snooping-credit-vidya_in_5d1cbc34e4b082e55373370a) - July 4, 2019, Huffington Post India
* [Quiet rise of India’s ticketing unicorn](https://www.livemint.com/companies/start-ups/quiet-rise-of-india-s-ticketing-unicorn-1563985480102.html) - July 24,2019, Livemint