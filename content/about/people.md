---
title: People
description: An incomplete list of people who have contributed to Cashless Consumer
date: 2018-03-13
updated: 2018-03-13
layout: subsection
slug: people
weight: 102
---

Through its journey, Cashless Consumer has had voluntary contributions from various people interested and alligned with goals of Cashless Consumer. Some of them help us by sharing their knowledge discreetly. This page (incompletely) lists the names of people who have helped and thank them for their contributions. Contributors are welcome and we follow [Wikipedia's guideline on voluntary work](https://en.wikipedia.org/wiki/WP:VOLUNTEER)

## Friends of Cashless Consumer
* Journalists following and reporting payment industry.
* Policy wonks
* Developers in fintech firms
* [50p](https://50p.in) and its community

## Contributors
* Shriram Bhat and all [contributors of initial site on UPI](https://github.com/CashlessConsumer/CashlessConsumer/graphs/contributors)
* Veevan V - artwork, logo
* Chinmayi SK - [Ethical Framework for evangelism of Cashless Consumer apps](https://commons.wikimedia.org/wiki/File:Ethicalframeworkforevangelisationofcashlessconsumerapps.pdf)
* Jyoti Panday
* Anivar Aravind
* Srinivas Kodali
* Srikanth L
